package com.babestudios.revolut.common

import android.content.res.Configuration
import android.content.res.Configuration.ORIENTATION_UNDEFINED
import com.airbnb.mvrx.BaseMvRxFragment

abstract class BaseFragment() : BaseMvRxFragment() {

	var orientation: Int = ORIENTATION_UNDEFINED

	override fun invalidate() {

	}

	abstract fun orientationChanged()

	/**
	 * This is a quick fix until nav graph lifecycle support is added to MvRx:
	 * @see [link](https://github.com/airbnb/MvRx/pull/351)
	 */
	override fun onConfigurationChanged(newConfig: Configuration) {
		super.onConfigurationChanged(newConfig)
		val currentOrientation = resources.configuration.orientation
		if (orientation != currentOrientation) {
			orientationChanged()
		}
		orientation = currentOrientation
	}
}
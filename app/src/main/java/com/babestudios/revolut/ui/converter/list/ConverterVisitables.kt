package com.babestudios.revolut.ui.converter.list

import com.babestudios.revolut.data.model.CurrencyQuote

sealed class BaseConverterVisitable {
	abstract fun type(converterTypeFactory: ConverterAdapter.ConverterTypeFactory): Int
}

class ConverterVisitable(val currencyQuote: CurrencyQuote) : BaseConverterVisitable() {
	override fun type(converterTypeFactory: ConverterAdapter.ConverterTypeFactory): Int {
		return converterTypeFactory.type(currencyQuote)
	}
}

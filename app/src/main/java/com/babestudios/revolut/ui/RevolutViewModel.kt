package com.babestudios.revolut.ui

import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.ViewModelContext
import com.babestudios.base.mvrx.BaseViewModel
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.revolut.data.OfflineException
import com.babestudios.revolut.data.RevolutRepositoryContract
import com.babestudios.revolut.data.model.CurrencyQuote
import com.babestudios.revolut.data.model.CurrencyRates
import com.babestudios.revolut.data.model.currencies
import com.babestudios.revolut.navigation.features.RevolutNavigator
import com.babestudios.revolut.ui.converter.list.BaseConverterVisitable
import com.babestudios.revolut.ui.converter.list.ConverterVisitable
import io.reactivex.disposables.Disposable

class RevolutViewModel(
	revolutState: RevolutState,
	private val revolutRepository: RevolutRepositoryContract,
	private var revolutNavigator: RevolutNavigator,
	private val schedulerProvider: SchedulerProvider
) : BaseViewModel<RevolutState>(revolutState, revolutRepository) {

	var disposable: Disposable? = null

	companion object : MvRxViewModelFactory<RevolutViewModel, RevolutState> {

		@JvmStatic
		override fun create(
			viewModelContext: ViewModelContext,
			state: RevolutState
		): RevolutViewModel? {
			val revolutRepositoryContract =
				viewModelContext.activity<RevolutActivity>().injectRevolutRepositoryContract()
			val revolutNavigator =
				viewModelContext.activity<RevolutActivity>().injectRevolutNavigator()
			val schedulerProvider =
				viewModelContext.activity<RevolutActivity>().injectSchedulerProvider()
			return RevolutViewModel(
				state,
				revolutRepositoryContract,
				revolutNavigator,
				schedulerProvider
			)
		}

		override fun initialState(viewModelContext: ViewModelContext): RevolutState? {
			val revolutRepositoryContract =
				viewModelContext.activity<RevolutActivity>().injectRevolutRepositoryContract()
			return RevolutState(baseCurrency = revolutRepositoryContract.baseCurrency())
		}
	}

	init {
		selectSubscribe(RevolutState::baseAmount) {
			if (disposable?.isDisposed == true) {
				fetchRates()
			}
		}
	}

	//region Rates

	fun fetchRates() {
		withState { state ->
			disposable = revolutRepository.fetchRates(state.baseCurrency)
				.compose(schedulerProvider.getSchedulersForObservable())
				.execute {
					val rates = when (it) {
						is Success<CurrencyRates> -> it().rates
						is Fail -> ((it.error as? OfflineException)?.obj as? CurrencyRates)?.rates ?: emptyMap()
						else -> emptyMap()
					}
					copy(
						ratesRequest = it,
						rates = rates,
						converterVisitables = convertToVisitables(
							rates,
							this.baseAmount,
							this.baseCurrency,
							this.converterVisitables
						)
					)
				}
		}
	}

	private fun convertToVisitables(
		rates: Map<String, Float>,
		baseAmount: Float,
		baseCurrency: String,
		converterVisitables: List<BaseConverterVisitable>
	): List<BaseConverterVisitable> {
		return when {
			converterVisitables.size <= 2 -> {
				val result = emptyList<ConverterVisitable>().toMutableList()
				val currencyMap = currencies.associateBy { it.currencyCode }
				rates.mapTo(result, {
					ConverterVisitable(
						CurrencyQuote(
							false,
							it.key,
							currencyMap[it.key]?.currencyName,
							currencyMap[it.key]?.currencyFlag,
							(it.value * baseAmount).round()
						)
					)
				})
				result.add(
					0, ConverterVisitable(
						CurrencyQuote(
							true,
							baseCurrency,
							currencyMap[baseCurrency]?.currencyName,
							currencyMap[baseCurrency]?.currencyFlag,
							baseAmount
						)
					)
				)
				result
			}
			else -> {
				updateVisitables(converterVisitables, rates, baseAmount)
			}
		}
	}

	private fun updateVisitables(
		converterVisitables: List<BaseConverterVisitable>,
		rates: Map<String, Float>,
		baseAmount: Float
	): List<ConverterVisitable> {
		return converterVisitables.map {
			val currencyQuote = (it as ConverterVisitable).currencyQuote
			if (currencyQuote.isBase) {
				it
			} else {
				ConverterVisitable(
					currencyQuote.copy(
						amount = (rates[currencyQuote.currencyCode]?.times(
							baseAmount
						))?.round() ?: 0f
					)
				)
			}
		}
	}

	private fun Float.round(): Float = "%.2f".format(this).toFloat()

//endregion

	fun setNavigator(navigator: RevolutNavigator) {
		revolutNavigator = navigator
	}

	fun baseAmountChanged(baseAmount: Float) {
		withState {
			if (it.baseAmount != baseAmount) {
				disposable?.dispose()
				setState {
					val updatedVisitables = updateVisitables(converterVisitables, rates, baseAmount)
					copy(
						baseAmount = baseAmount,
						converterVisitables = updatedVisitables
					)
				}
			}
		}
	}

	fun changeBaseCurrencyClicked(adapterPosition: Int) {
		withState {
			if (adapterPosition > 0 && it.ratesRequest !is Fail) {
				disposable?.dispose()
				setState {
					val visitables = this.converterVisitables.toMutableList()
					val oldBase = visitables[0] as ConverterVisitable
					val oldBaseCurrency = oldBase.currencyQuote.copy(isBase = false)
					val newBase = visitables.removeAt(adapterPosition) as ConverterVisitable
					val newBaseCurrency = newBase.currencyQuote.copy(isBase = true)
					revolutRepository.setBaseCurrency(newBaseCurrency.currencyCode)
					visitables[0] = ConverterVisitable(oldBaseCurrency)
					visitables.add(0, ConverterVisitable(newBaseCurrency))
					copy(
						converterVisitables = visitables,
						baseAmount = newBaseCurrency.amount,
						baseCurrency = newBaseCurrency.currencyCode
					)
				}
			}
		}
	}

}

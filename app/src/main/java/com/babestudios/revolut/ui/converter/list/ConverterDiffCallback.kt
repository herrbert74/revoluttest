package com.babestudios.revolut.ui.converter.list

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil


class ConverterDiffCallback(
	private val oldList: List<BaseConverterVisitable>,
	private val newList: List<BaseConverterVisitable>
) : DiffUtil.Callback() {

	override fun getOldListSize(): Int = oldList.size

	override fun getNewListSize(): Int = newList.size

	override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
		val oldCurrencyRate = (oldList[oldItemPosition] as ConverterVisitable).currencyQuote
		val newCurrencyRate = (newList[newItemPosition] as ConverterVisitable).currencyQuote
		return oldCurrencyRate.currencyCode == newCurrencyRate.currencyCode
	}

	override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
		val oldCurrencyRate = (oldList[oldItemPosition] as ConverterVisitable).currencyQuote
		val newCurrencyRate = (newList[newItemPosition] as ConverterVisitable).currencyQuote
		return (
				//Base currency haven't changed -> ignore amount changes
				oldCurrencyRate.isBase && newCurrencyRate.isBase &&
						oldCurrencyRate.currencyCode == newCurrencyRate.currencyCode
				) ||
				//Other cases
				(oldCurrencyRate.isBase == newCurrencyRate.isBase &&
						oldCurrencyRate.amount == newCurrencyRate.amount)
	}

	@Nullable
	override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
		return (newList[newPosition] as ConverterVisitable).currencyQuote
	}
}
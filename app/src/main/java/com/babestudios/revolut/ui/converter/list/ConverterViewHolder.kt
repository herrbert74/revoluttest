package com.babestudios.revolut.ui.converter.list

import androidx.viewbinding.ViewBinding
import com.babestudios.base.list.BaseViewHolder
import com.babestudios.revolut.data.model.CurrencyQuote
import com.babestudios.revolut.databinding.ItemRateBinding


class ConverterViewHolder(_binding: ViewBinding) :
	BaseViewHolder<BaseConverterVisitable>(_binding) {
	override fun bind(visitable: BaseConverterVisitable) {
		val binding = _binding as ItemRateBinding
		val currencyRate = (visitable as ConverterVisitable).currencyQuote
		binding.lblRateItemFlag.text = currencyRate.currencyFlag
		binding.lblRateItemCurrencyCode.text = currencyRate.currencyCode
		binding.lblRateItemCurrencyName.text = currencyRate.currencyName
		binding.etRateItemQuote.setText(currencyRate.amount.toString().trim())
		binding.etRateItemQuote.isEnabled = currencyRate.isBase
	}

	fun bind(currencyQuote: CurrencyQuote) {
		val binding = _binding as ItemRateBinding
		binding.etRateItemQuote.setText(currencyQuote.amount.toString())
		binding.etRateItemQuote.isEnabled = currencyQuote.isBase
	}
}

package com.babestudios.revolut.ui

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.babestudios.base.ext.isLazyInitialized
import com.babestudios.base.mvrx.BaseActivity
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.revolut.R
import com.babestudios.revolut.data.RevolutRepositoryContract
import com.babestudios.revolut.di.AppInjectHelper
import com.babestudios.revolut.navigation.features.RevolutNavigator

class RevolutActivity: BaseActivity() {

	private val comp by lazy {
		AppInjectHelper.provideAppComponent(applicationContext)
	}

	private lateinit var revolutNavigator: RevolutNavigator

	private lateinit var navController: NavController

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_revolut)
		navController = findNavController(R.id.navHostFragmentRevolut)
		if (::comp.isLazyInitialized) {
			revolutNavigator.bind(navController)
		}
	}

	override fun onBackPressed() {
		if (onBackPressedDispatcher.hasEnabledCallbacks()) {
			onBackPressedDispatcher.onBackPressed()
		} else {
			super.finish()
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out)
		}
	}

	fun injectRevolutRepositoryContract(): RevolutRepositoryContract {
		return comp.revolutRepository()
	}

	fun injectSchedulerProvider(): SchedulerProvider {
		return comp.schedulerProvider()
	}

	fun injectRevolutNavigator(): RevolutNavigator {
		revolutNavigator = comp.navigator()
		if (::navController.isInitialized)
			revolutNavigator.bind(navController)
		return revolutNavigator
	}
}

package com.babestudios.revolut.ui.converter.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.babestudios.base.list.BaseViewHolder
import com.babestudios.revolut.R
import com.babestudios.revolut.data.model.CurrencyQuote
import com.babestudios.revolut.databinding.ItemRateBinding
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class ConverterAdapter(
	private var converterVisitables: List<BaseConverterVisitable>
	, private val converterTypeFactory: ConverterTypeFactory
) : RecyclerView.Adapter<BaseViewHolder<BaseConverterVisitable>>() {

	override fun getItemCount(): Int {
		return converterVisitables.size
	}

	override fun getItemViewType(position: Int): Int {
		return converterVisitables[position].type(converterTypeFactory)
	}

	private val amountChangeClickSubject = PublishSubject.create<CharSequence>()

	fun amountChangedObservable(): Observable<CharSequence> {
		return amountChangeClickSubject
	}

	private val changeBaseCurrencyClickSubject =
		PublishSubject.create<BaseViewHolder<BaseConverterVisitable>>()

	fun changeBaseCurrencyClickedObservable(): Observable<BaseViewHolder<BaseConverterVisitable>> {
		return changeBaseCurrencyClickSubject
	}

	interface ConverterTypeFactory {
		fun type(currencyQuote: CurrencyQuote): Int
		fun holder(type: Int, binding: ViewBinding): BaseViewHolder<*>
	}

	override fun onCreateViewHolder(
		parent: ViewGroup,
		viewType: Int
	): BaseViewHolder<BaseConverterVisitable> {
		val binding = ItemRateBinding.inflate(
			LayoutInflater.from(parent.context),
			parent,
			false
		)
		val v = converterTypeFactory.holder(viewType, binding) as ConverterViewHolder
		RxTextView.textChanges(binding.etRateItemQuote)
			.skipInitialValue()
			.debounce(
				parent.resources.getInteger(R.integer.inputFieldDebounce).toLong(),
				TimeUnit.MILLISECONDS
			)
			.takeUntil(RxView.detaches(parent))
			.filter { v.layoutPosition == 0 }
			.subscribe(amountChangeClickSubject)
		RxView.clicks(binding.root)
			.takeUntil(RxView.detaches(parent))
			.map { v }
			.subscribe(changeBaseCurrencyClickSubject)
		return v
	}

	override fun onBindViewHolder(holder: BaseViewHolder<BaseConverterVisitable>, position: Int) {
		holder.bind(converterVisitables[position])
	}

	override fun onBindViewHolder(
		holder: BaseViewHolder<BaseConverterVisitable>,
		position: Int,
		payloads: MutableList<Any>
	) {
		if (payloads.isEmpty()) holder.bind(converterVisitables[position])
		else (holder as ConverterViewHolder).bind(payloads[0] as CurrencyQuote)
	}

	fun updateItems(visitables: List<BaseConverterVisitable>) {
		val diffCallback = ConverterDiffCallback(converterVisitables, visitables)
		val diffResult = DiffUtil.calculateDiff(diffCallback)
		converterVisitables = visitables
		diffResult.dispatchUpdatesTo(this)
	}
}

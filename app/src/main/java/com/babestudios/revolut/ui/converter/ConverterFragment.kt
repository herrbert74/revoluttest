package com.babestudios.revolut.ui.converter

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.mvrx.*
import com.babestudios.base.list.BaseViewHolder
import com.babestudios.base.view.DividerItemDecoration
import com.babestudios.base.view.MultiStateView.*
import com.babestudios.revolut.R
import com.babestudios.revolut.common.BaseFragment
import com.babestudios.revolut.data.OfflineException
import com.babestudios.revolut.databinding.FragmentConverterBinding
import com.babestudios.revolut.ui.RevolutActivity
import com.babestudios.revolut.ui.RevolutState
import com.babestudios.revolut.ui.RevolutViewModel
import com.babestudios.revolut.ui.converter.list.BaseConverterVisitable
import com.babestudios.revolut.ui.converter.list.ConverterAdapter
import com.babestudios.revolut.ui.converter.list.ConverterTypeFactory
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_converter.*


class ConverterFragment : BaseFragment() {

	private var converterAdapter: ConverterAdapter? = null

	private val viewModel by activityViewModel(RevolutViewModel::class)

	private val eventDisposables: CompositeDisposable = CompositeDisposable()

	private var _binding: FragmentConverterBinding? = null
	private val binding get() = _binding!!

	//region life cycle

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		selectSubscribes()
	}

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		_binding = FragmentConverterBinding.inflate(inflater, container, false)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initializeUI()
	}

	private fun initializeUI() {
		viewModel.logScreenView(this::class.simpleName.orEmpty())
		binding.tbRates.title = getString(R.string.rates_title)
		(activity as AppCompatActivity).setSupportActionBar(binding.tbRates)
		createRatesRecyclerView()
		viewModel.fetchRates()
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}

	private fun createRatesRecyclerView() {
		val linearLayoutManager =
			LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		binding.rvRates.layoutManager = linearLayoutManager
		binding.rvRates.addItemDecoration(DividerItemDecoration(requireContext()))
	}

	override fun orientationChanged() {
		val activity = requireActivity() as RevolutActivity
		viewModel.setNavigator(activity.injectRevolutNavigator())
	}

	//endregion

	//region render

	private fun selectSubscribes() {

		viewModel.selectSubscribe(RevolutState::converterVisitables) { converterVisitables ->
			val tvMsvError = binding.msvRates.findViewById<TextView>(R.id.tvMsvError)
			withState(viewModel) { state ->
				when (state.ratesRequest) {
					is Uninitialized -> {
					}
					is Loading -> {
						if (binding.rvRates.adapter == null) binding.msvRates.viewState =
							VIEW_STATE_LOADING
						Unit
					}
					is Fail -> {
						if (state.ratesRequest.error is OfflineException) {
							showResult(state.converterVisitables)
							binding.llRatesCannotConnect.visibility = View.VISIBLE
						} else {
							binding.msvRates.viewState = VIEW_STATE_ERROR
							tvMsvError.text = state.ratesRequest.error.message
						}
					}
					is Success -> {
						val tvMsvEmpty = binding.msvRates.findViewById<TextView>(R.id.tvMsvEmpty)
						if (converterVisitables.isEmpty()) {
							binding.msvRates.viewState = VIEW_STATE_EMPTY
							tvMsvEmpty.text =
								getString(R.string.no_rates)
							observeActions()
						} else {
							showResult(state.converterVisitables)
						}
					}
				}
			}
		}
	}

	private fun showResult(it: List<BaseConverterVisitable>): Unit? {
		binding.rvRates.visibility = View.VISIBLE
		binding.msvRates.viewState = VIEW_STATE_CONTENT
		return if (binding.rvRates.adapter == null) {
			converterAdapter = ConverterAdapter(it, ConverterTypeFactory())
			binding.rvRates.adapter = converterAdapter
			//We skip initial value for base amount
			rvRates.viewTreeObserver
				.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
					override fun onGlobalLayout() {
						rvRates.viewTreeObserver.removeOnGlobalLayoutListener(this)
						Handler().postDelayed(
							{ observeActions() },
							resources.getInteger(R.integer.inputFieldDebounce).toLong()
						)
					}
				})
			converterAdapter?.registerAdapterDataObserver(object :
				RecyclerView.AdapterDataObserver() {
				override fun onItemRangeMoved(
					fromPosition: Int,
					toPosition: Int,
					itemCount: Int
				) {
					rvRates.scrollToPosition(toPosition)
				}
			})
		} else {
			converterAdapter?.updateItems(it)
		}
	}

//endregion

//region events

	private fun observeActions() {
		eventDisposables.clear()
		RxView.clicks(binding.btnRatesReload)
			.subscribe {
				binding.llRatesCannotConnect.visibility = View.GONE
				viewModel.fetchRates()
			}
			?.let { eventDisposables.add(it) }
		converterAdapter?.amountChangedObservable()
			?.subscribe { charSequence: CharSequence ->
				withState(viewModel) { state ->
					state.converterVisitables.let {
						viewModel.baseAmountChanged(charSequence.toString().toFloatOrNull() ?: 0f)
					}
				}
			}
			?.let { eventDisposables.add(it) }
		converterAdapter?.changeBaseCurrencyClickedObservable()
			?.subscribe { view: BaseViewHolder<BaseConverterVisitable> ->
				withState(viewModel) { state ->
					state.converterVisitables.let {
						viewModel.changeBaseCurrencyClicked(view.adapterPosition)
					}
				}
			}
			?.let { eventDisposables.add(it) }
	}

//endregion

}

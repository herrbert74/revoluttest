package com.babestudios.revolut.ui

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.Uninitialized
import com.babestudios.revolut.data.model.CurrencyRates
import com.babestudios.revolut.ui.converter.list.BaseConverterVisitable

data class RevolutState(
	val baseCurrency: String = "EUR",
	val baseAmount: Float = 100.00f,
	val ratesRequest: Async<CurrencyRates> = Uninitialized,
	val rates: Map<String, Float> = emptyMap(),
	val converterVisitables: List<BaseConverterVisitable> = emptyList()
) : MvRxState
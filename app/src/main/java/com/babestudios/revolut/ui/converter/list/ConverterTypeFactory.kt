package com.babestudios.revolut.ui.converter.list

import androidx.viewbinding.ViewBinding
import com.babestudios.base.list.BaseViewHolder
import com.babestudios.revolut.R
import com.babestudios.revolut.data.model.CurrencyQuote

class ConverterTypeFactory :
	ConverterAdapter.ConverterTypeFactory {
	override fun type(currencyQuote: CurrencyQuote): Int = R.layout.item_rate

	override fun holder(type: Int, binding: ViewBinding): BaseViewHolder<*> {
		return when (type) {
			R.layout.item_rate -> ConverterViewHolder(binding)
			else -> throw IllegalStateException("Illegal view type")
		}
	}
}

package com.babestudios.revolut.navigation.features

import com.babestudios.revolut.navigation.base.Navigator

interface RevolutNavigator : Navigator {
	fun fromAToB()
}

package com.babestudios.revolut

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import com.babestudios.base.mvrx.LifeCycleApp
import com.babestudios.revolut.di.AppComponent
import com.babestudios.revolut.di.AppComponentProvider
import com.babestudios.revolut.di.AppModule
import com.babestudios.revolut.di.DaggerAppComponent
import com.babestudios.revolut.ui.AppNavigation

open class App : Application(), AppComponentProvider, LifeCycleApp {

	private var currentActivity: AppCompatActivity? = null

	private lateinit var appComponent: AppComponent

	override fun onCreate() {
		super.onCreate()
		logAppOpen()
	}

	private fun logAppOpen() {
		provideAppComponent().revolutRepository().logAppOpen()
	}

	override fun provideAppComponent(): AppComponent {

		if (!this::appComponent.isInitialized) {
			appComponent = DaggerAppComponent
				.factory()
				.create(AppModule(this), AppNavigation(), this)
		}
		return appComponent
	}

	override fun getCurrentActivity(): AppCompatActivity? {
		return currentActivity
	}

	override fun setCurrentActivity(mCurrentActivity: AppCompatActivity) {
		this.currentActivity = mCurrentActivity
	}
}
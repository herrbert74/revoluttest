package com.babestudios.revolut.data

import com.babestudios.base.rxjava.ErrorResolver
import com.babestudios.revolut.Database
import com.babestudios.revolut.RateQueries
import com.babestudios.revolut.data.dto.CurrencyRatesDto
import com.babestudios.revolut.data.model.CurrencyRates
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class RevolutErrorResolver @Inject constructor(private val database: Database) : ErrorResolver {

	/**
	 * Not applicable for this project
	 */
	override fun errorMessageFromResponseObject(errorObject: Any?): String? {
		return null
	}

	override fun errorMessageFromResponseBody(responseBody: ResponseBody): String? {
		return try {
			val jsonObject = JSONObject(responseBody.string())
			jsonObject.getString("message")
		} catch (e: Exception) {
			"An error happened, try again."
		}
	}

	@Suppress("RemoveExplicitTypeArguments")
	override fun <T> resolveErrorForSingle(): (Single<T>) -> Single<T> {
		return { single ->
			single.onErrorResumeNext {
				(it as? HttpException)?.response()?.errorBody()?.let { body ->
					Single.error<T> { Exception(errorMessageFromResponseBody(body)) }
				} ?: Single.error<T> { Exception("An error happened") }
			}
		}
	}

	@Suppress("RemoveExplicitTypeArguments")
	fun resolveErrorForRatesSingle(): (Single<CurrencyRatesDto>) -> Single<CurrencyRatesDto> {
		return { single ->
			single.onErrorResumeNext {
				val m = getRatesFromStorage()
				Single.error(OfflineException(getRatesFromStorage()))
			}
		}
	}

	@Suppress("RemoveExplicitTypeArguments", "RemoveRedundantQualifierName")
	fun resolveErrorForRatesObservable(): (Observable<CurrencyRates>) -> Observable<CurrencyRates> {
		return { observable ->
			@Suppress("RedundantLambdaArrow")
			observable.onErrorResumeNext { _: Throwable ->
				Observable.error<CurrencyRates>(OfflineException(getRatesFromStorage()))
			}
		}
	}


	fun getRatesFromStorageSingle(): Single<CurrencyRates> {
		return Single.error(
			OfflineException(
				getRatesFromStorage()
			)
		)
	}

	private fun getRatesFromStorage(): CurrencyRates {
		val rateQueries: RateQueries = database.rateQueries
		return CurrencyRates(
			rateQueries.selectAll { t, z ->
				t to z
			}.executeAsList().toMap()
		)
	}

}

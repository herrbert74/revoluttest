package com.babestudios.revolut.data.dto

import com.babestudios.revolut.data.model.CurrencyRates


data class CurrencyRatesDto(
	val baseCurrency: String,
	val rates: Map<String, Float>
)

fun CurrencyRatesDto.convertToModel() :CurrencyRates {
	return CurrencyRates(this.rates)
}
package com.babestudios.revolut.data

class OfflineException(val obj: Any): Exception()
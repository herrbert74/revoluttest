package com.babestudios.revolut.data

import com.babestudios.revolut.data.dto.CurrencyRatesDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutService {

	@GET("latest")
	fun fetchRates(@Query("base") base: String): Single<CurrencyRatesDto>
}
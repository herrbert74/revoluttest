package com.babestudios.revolut.data.model

data class CurrencyQuote(
	val isBase: Boolean = false,
	val currencyCode: String,
	val currencyName: String? = null,
	val currencyFlag: String? = null,
	val amount: Float = 0f
)

data class Currency(
	val currencyCode: String,
	val currencyName: String,
	val currencyFlag: String
)

val currencies: List<Currency> = listOf(
	Currency(
		"EUR",
		"Euro",
		"🇪🇺"
	),
	Currency(
		"AUD",
		"Australia Dollar",
		"🇦🇺"
	),
	Currency(
		"BGN",
		"Bulgaria Lev",
		"🇧🇬"
	),
	Currency(
		"BRL",
		"Brazil Real",
		"🇧🇷"
	),
	Currency(
		"CAD",
		"Canada Dollar",
		"🇨🇦"
	),
	Currency(
		"CHF",
		"Switzerland Franc",
		"🇨🇭"
	),
	Currency(
		"CNY",
		"China Yuan Renminbi",
		"🇨🇳"
	),
	Currency(
		"CZK",
		"Czech Republic Koruna",
		"🇨🇿"
	),
	Currency(
		"DKK",
		"Denmark Krone",
		"🇩🇰"
	),
	Currency(
		"GBP",
		"United Kingdom Pound",
		"🇬🇧"
	),
	Currency(
		"HKD",
		"Hong Kong Dollar",
		"🇭🇰"
	),
	Currency(
		"HRK",
		"Croatia Kuna",
		"🇭🇷"
	),
	Currency(
		"HUF",
		"Hungary Forint",
		"🇭🇺"
	),
	Currency(
		"IDR",
		"Indonesia Rupiah",
		"🇮🇩"
	),
	Currency(
		"ILS",
		"Israel Shekel",
		"🇮🇱"
	),
	Currency(
		"INR",
		"India Rupee",
		"🇮🇳"
	),
	Currency(
		"ISK",
		"Iceland Krona",
		"🇮🇸"
	),
	Currency(
		"JPY",
		"Japan Yen",
		"🇯🇵"
	),
	Currency(
		"KRW",
		"Korea (South) Won",
		"🇰🇷"
	),
	Currency(
		"MXN",
		"Mexico Peso",
		"🇲🇽"
	),
	Currency(
		"MYR",
		"Malaysia Ringgit",
		"🇲🇾"
	),
	Currency(
		"NOK",
		"Norway Krone",
		"🇳🇴"
	),
	Currency(
		"NZD",
		"New Zealand Dollar",
		"🇳🇿"
	),
	Currency(
		"PHP",
		"Philippines Peso",
		"🇵🇭"
	),
	Currency(
		"PLN",
		"Poland Zloty",
		"🇵🇱"
	),
	Currency(
		"RON",
		"Romania Leu",
		"🇷🇴"
	),
	Currency(
		"RUB",
		"Russia Ruble",
		"🇷🇺"
	),
	Currency(
		"SEK",
		"Sweden Krona",
		"🇸🇪"
	),
	Currency(
		"SGD",
		"Singapore Dollar",
		"🇸🇬"
	),
	Currency(
		"THB",
		"Thailand Baht",
		"🇹🇭"
	),
	Currency(
		"USD",
		"United States Dollar",
		"🇺🇸"
	),
	Currency(
		"ZAR",
		"South Africa Rand",
		"🇿🇦"
	)
)
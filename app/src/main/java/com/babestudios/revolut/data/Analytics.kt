package com.babestudios.revolut.data

import com.babestudios.base.data.AnalyticsContract

class Analytics : AnalyticsContract {
	override fun logAppOpen() {
		//Empty
	}

	override fun logScreenView(screenName: String) {
		//Empty
	}

	override fun logSearch(queryText: String) {
		//Empty
	}

}
package com.babestudios.revolut.data

import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.babestudios.base.data.AnalyticsContract
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.revolut.Database
import com.babestudios.revolut.RateQueries
import com.babestudios.revolut.data.dto.convertToModel
import com.babestudios.revolut.data.model.CurrencyRates
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface RevolutRepositoryContract : AnalyticsContract {
	//network calls
	fun fetchRates(baseCurrency: String): Observable<CurrencyRates>

	//preferences
	fun baseCurrency(): String
	fun setBaseCurrency(baseCurrency: String)
}

class RevolutRepository @Inject constructor(
	private val revolutService: RevolutService,
	private val analytics: AnalyticsContract,
	private val schedulerProvider: SchedulerProvider,
	private val database: Database,
	private val connectivityManager: ConnectivityManager,
	private val errorResolver: RevolutErrorResolver,
	private val preferencesHelper: PreferencesHelper
) : RevolutRepositoryContract {


	override fun fetchRates(baseCurrency: String): Observable<CurrencyRates> {
		return when {
			isNetworkAvailable() -> Observable.interval(0L, 1L, TimeUnit.SECONDS)
				.flatMapSingle {
					fetchRatesSingle(baseCurrency)
				}
				.compose(errorResolver.resolveErrorForRatesObservable())
			else -> {
				errorResolver.getRatesFromStorageSingle().toObservable()
			}
		}
	}

	private fun fetchRatesSingle(baseCurrency: String): Single<CurrencyRates> {
		return when {
			isNetworkAvailable() ->
				revolutService
					.fetchRates(baseCurrency)
					.compose(errorResolver.resolveErrorForRatesSingle())
					.map {
						it.convertToModel()
					}
					.doOnSuccess { currencyRates ->
						val rateQueries: RateQueries = database.rateQueries
						rateQueries.deleteAll()
						currencyRates.rates.forEach {
							rateQueries.insertRate(it.key, it.value)
						}
					}
					.compose(schedulerProvider.getSchedulersForSingle())
			else -> {
				errorResolver.getRatesFromStorageSingle()
			}
		}
	}

	//region analytics

	override fun logAppOpen() {
		analytics.logAppOpen()
	}

	override fun logScreenView(screenName: String) {
		analytics.logScreenView(screenName)
	}

	override fun logSearch(queryText: String) {
		analytics.logSearch(queryText)
	}

	//endregion

	// region Preferences

	override fun baseCurrency(): String {
		return preferencesHelper.baseCurrency
	}

	override fun setBaseCurrency(baseCurrency: String) {
		preferencesHelper.setBaseCurrency(baseCurrency)
	}

	//endregion

	// region Utils

	@Suppress("DEPRECATION")
	private fun isNetworkAvailable(): Boolean {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			val nw = connectivityManager.activeNetwork ?: return false
			val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
			return when {
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
				//for other device how are able to connect with Ethernet
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
				//for check internet over Bluetooth
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
				else -> false
			}
		} else {
			val nwInfo = connectivityManager.activeNetworkInfo ?: return false
			return nwInfo.isConnected
		}
	}

	//endregion
}
package com.babestudios.revolut.data.model


data class CurrencyRates(
	val rates: Map<String, Float>
)

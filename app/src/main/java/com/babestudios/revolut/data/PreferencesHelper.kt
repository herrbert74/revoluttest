package com.babestudios.revolut.data

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

const val PREF_FILE_NAME = "pref_file"
const val PREF_BASE_CURRENCY = "revolut_base_currency"

private const val DEFAULT_BASE_CURRENCY = "EUR"

@Singleton
class PreferencesHelper
@Inject internal constructor(
	private val sharedPreferences: SharedPreferences
) {

	fun setBaseCurrency(baseCurrency: String) {
		sharedPreferences.edit().putString(PREF_BASE_CURRENCY, baseCurrency).apply()
	}

	val baseCurrency: String
		get() {
			return sharedPreferences.getString(PREF_BASE_CURRENCY, DEFAULT_BASE_CURRENCY) ?: DEFAULT_BASE_CURRENCY
		}

}

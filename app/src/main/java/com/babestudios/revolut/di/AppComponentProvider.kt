package com.babestudios.revolut.di

interface AppComponentProvider {
	fun provideAppComponent(): AppComponent
}

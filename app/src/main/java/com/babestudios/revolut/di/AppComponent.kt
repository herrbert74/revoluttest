package com.babestudios.revolut.di

import android.content.Context
import com.babestudios.base.di.qualifier.ApplicationContext
import com.babestudios.base.rxjava.ErrorResolver
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.revolut.data.RevolutRepositoryContract
import com.babestudios.revolut.navigation.di.NavigationComponent
import com.babestudios.revolut.navigation.features.RevolutNavigator
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, AppBinderModule::class],
	dependencies = [NavigationComponent::class])
interface AppComponent {

	@Component.Factory
	interface Factory {
		fun create(
			appModule: AppModule,
			navigationComponent: NavigationComponent,
			@BindsInstance @ApplicationContext applicationContext: Context
		): AppComponent
	}

	fun revolutRepository(): RevolutRepositoryContract

	fun schedulerProvider(): SchedulerProvider

	fun errorResolver(): ErrorResolver

	fun navigator(): RevolutNavigator

	@ApplicationContext
	fun context(): Context
}

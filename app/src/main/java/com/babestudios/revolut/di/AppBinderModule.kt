package com.babestudios.revolut.di

import com.babestudios.base.rxjava.ErrorResolver
import com.babestudios.revolut.data.RevolutErrorResolver
import com.babestudios.revolut.data.RevolutRepository
import com.babestudios.revolut.data.RevolutRepositoryContract
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Using the [Binds] annotation to bind internal implementations to their interfaces results in less generated code.
 * We also get rid of actually using this module by turning it into an abstract class, then an interface,
 * hence it's separated from [AppModule]
 */
@Module
interface AppBinderModule {

	@Singleton
	@Binds
	fun bindRevolutRepositoryContract(revolutRepository: RevolutRepository): RevolutRepositoryContract

	@Singleton
	@Binds
	fun provideErrorResolver(revolutErrorResolver: RevolutErrorResolver): ErrorResolver
}

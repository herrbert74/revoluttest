package com.babestudios.revolut.di

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.AsyncTask
import com.babestudios.base.data.AnalyticsContract
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.revolut.BuildConfig
import com.babestudios.revolut.Database
import com.babestudios.revolut.data.Analytics
import com.babestudios.revolut.data.PREF_FILE_NAME
import com.babestudios.revolut.data.RevolutService
import com.babestudios.revolut.data.converters.AdvancedGsonConverterFactory
import com.babestudios.revolut.data.interceptors.EmptyBodyInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.inject.Singleton

@Module
@Suppress("unused")
open class AppModule(private val context: Context) {

	@Provides
	@Singleton
	internal fun provideRevolutRetrofit(): Retrofit {
		val logging = HttpLoggingInterceptor()
		logging.level = HttpLoggingInterceptor.Level.BODY

		val httpClient = OkHttpClient.Builder()
		httpClient.addInterceptor(logging)
		httpClient.addInterceptor(EmptyBodyInterceptor())
		return Retrofit.Builder()//
			.baseUrl(BuildConfig.REVOLUT_BASE_URL)//
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())//
			.addConverterFactory(AdvancedGsonConverterFactory.create())//
			.client(httpClient.build())//
			.build()
	}

	@Provides
	@Singleton
	internal fun provideRevolutService(retroFit: Retrofit)
			: RevolutService {
		return retroFit.create(RevolutService::class.java)
	}

	@Provides
	internal fun provideGson(): Gson {
		return GsonBuilder()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz")
			.create()
	}

	@Provides
	@Singleton
	internal fun provideSchedulerProvider(): SchedulerProvider {
		return SchedulerProvider(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR), AndroidSchedulers.mainThread())
	}

	@Provides
	@Singleton
	internal fun provideAnalytics(): AnalyticsContract {
		return Analytics()
	}

	@Provides
	@Singleton
	internal fun provideDatabase(driver: AndroidSqliteDriver): Database {
		return Database(driver)
	}

	@Provides
	@Singleton
	internal fun provideSqlDriver(): AndroidSqliteDriver {
		return AndroidSqliteDriver(Database.Schema, context, "Rate.db")
	}

	@Provides
	@Singleton
	internal fun provideConnectivityManager(): ConnectivityManager {
		return context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
	}

	@Provides
	@Singleton
	internal fun provideSharedPreferences(): SharedPreferences {
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
	}

}
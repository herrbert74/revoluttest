package com.babestudios.revolut

import com.airbnb.mvrx.test.MvRxTestRule
import com.airbnb.mvrx.withState
import com.babestudios.base.ext.getPrivateFieldWithReflection
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.revolut.data.RevolutRepositoryContract
import com.babestudios.revolut.data.model.CurrencyRates
import com.babestudios.revolut.navigation.features.RevolutNavigator
import com.babestudios.revolut.ui.RevolutState
import com.babestudios.revolut.ui.RevolutViewModel
import com.babestudios.revolut.ui.converter.list.ConverterVisitable
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test

class ConverterViewModelTest {

	private val revolutRepositoryContract = mockk<RevolutRepositoryContract>(relaxed = true)

	private val revolutNavigator = mockk<RevolutNavigator>()

	private fun currencyRates() = CurrencyRates(
		rates = hashMapOf(Pair("AUD", 1.583f), Pair("BGN", 1.957f), Pair("BRL", 4.232f))
	)

	private fun currencyRatesUpdated() = CurrencyRates(
		rates = hashMapOf(Pair("AUD", 1.571f), Pair("BGN", 1.959f), Pair("BRL", 4.498f))
	)

	private fun currencyRatesNewBase() = CurrencyRates(
		rates = hashMapOf(Pair("BGN", 1.26f), Pair("BRL", 2.961f), Pair("EUR", 0.643f))
	)

	private var viewModel: RevolutViewModel? = null

	@Before
	fun setUp() {
		viewModel = revolutViewModel()
		every {
			revolutNavigator.fromAToB()
		} answers
				{
					Exception("")
				}
		every {
			revolutRepositoryContract.fetchRates("EUR")
		} answers {
			Observable.just(currencyRates())
		}
		every {
			revolutRepositoryContract.fetchRates("AUD")
		} answers {
			Observable.just(currencyRatesNewBase())
		}
	}


	@Test
	fun `when fetchRates is called then visitables and amounts are correct`() {
		viewModel?.fetchRates()
		withState(viewModel!!) {
			assert(it.converterVisitables.size == 4)
			assert((it.converterVisitables[1] as ConverterVisitable).currencyQuote.amount == 158.3f)
		}
	}

	@Test
	fun `when base amount has changed then fetchRates is called and amounts changed`() {
		viewModel?.fetchRates()
		viewModel?.baseAmountChanged(200f)
		val repo = viewModel?.getPrivateFieldWithReflection<RevolutRepositoryContract>("revolutRepository")
		verify(exactly = 2) { repo?.fetchRates("EUR") }
		withState(viewModel!!) {
			assert((it.converterVisitables[1] as ConverterVisitable).currencyQuote.amount == 316.6f)
		}
	}

	@Test
	fun `when rates have changed then amounts are updated`() {
		viewModel?.fetchRates()
		every {
			revolutRepositoryContract.fetchRates("EUR")
		} answers {
			Observable.just(currencyRatesUpdated())
		}
		viewModel?.fetchRates()
		withState(viewModel!!) {
			assert((it.converterVisitables[1] as ConverterVisitable).currencyQuote.amount == 157.1f)
		}
	}

	@Test
	fun `when currency clicked then visitables are updated`() {
		viewModel?.fetchRates()
		viewModel?.changeBaseCurrencyClicked(1)
		withState(viewModel!!) {
			assert((it.converterVisitables[0] as ConverterVisitable).currencyQuote.currencyCode == "AUD")
			assert((it.converterVisitables[1] as ConverterVisitable).currencyQuote.currencyCode == "EUR")
			assert((it.converterVisitables[1] as ConverterVisitable).currencyQuote.amount == 101.79f)
		}
	}

	private fun revolutViewModel(): RevolutViewModel {
		val schedulerProvider = SchedulerProvider(Schedulers.trampoline(), Schedulers.trampoline())
		return RevolutViewModel(
			RevolutState(
				converterVisitables = emptyList()
			),
			revolutRepositoryContract,
			revolutNavigator,
			schedulerProvider
		)
	}

	companion object {
		@JvmField
		@ClassRule
		val mvrxTestRule = MvRxTestRule()
	}
}

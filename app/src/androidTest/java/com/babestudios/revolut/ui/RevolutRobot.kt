package com.babestudios.revolut.ui

import androidx.core.view.size
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import com.azimolabs.conditionwatcher.ConditionWatcher
import com.azimolabs.conditionwatcher.Instruction
import com.babestudios.revolut.R
import com.babestudios.revolut.TestApp
import com.babestudios.revolut.testhelpers.matchers.ViewGroupMatchers
import org.hamcrest.CoreMatchers.allOf

class RevolutRobot(private val debounce: Long) {

	//region Converter screen

	fun setAmount(): RevolutRobot {
		onView(CONVERTER_FRAGMENT_EDITTEXT_MATCHER).perform(ViewActions.replaceText("12.4"))
		Thread.sleep(debounce)
		return this
	}

	fun clickRateItem(): RevolutRobot {
		ConditionWatcher.waitForCondition(CurrencyListInstruction())
		Thread.sleep(debounce) //Corresponding delay in selectSubscribes()
		onView(CONVERTER_FRAGMENT_ITEM_MATCHER).perform(click())
		Thread.sleep(debounce)
		return this
	}

	fun assertCurrencyListIsDisplayed(): RevolutRobot {
		ConditionWatcher.waitForCondition(CurrencyListInstruction())
		val textView = onView(CONVERTER_FRAGMENT_CURRENCY_CODE_MATCHER)
		textView.check(matches(withText("EUR")))
		return this
	}

	fun assertRatesAreUpdated(): RevolutRobot {
		ConditionWatcher.waitForCondition(CurrencyListInstruction())
		val textView = onView(CONVERTER_FRAGMENT_AMOUNT_MATCHER)
		Thread.sleep(debounce)
		textView.check(matches(withText("19.63")))
		return this
	}

	fun assertVisitablesReordered(): RevolutRobot {
		val textView = onView(CONVERTER_FRAGMENT_CURRENCY_CODE_MATCHER)
		textView.check(matches(withText("AUD")))
		return this
	}

	//endregion

	companion object {
		private val CONVERTER_FRAGMENT_EDITTEXT_MATCHER =
			allOf(
				withId(R.id.etRateItemQuote),
				isEnabled(),
				isDisplayed()
			)
		private val CONVERTER_FRAGMENT_ITEM_MATCHER =
			allOf(
				withId(R.id.clRateItem),
				ViewGroupMatchers.childAtPosition(
					withId(R.id.rvRates),
					1
				),
				isDisplayed()
			)
		private val CONVERTER_FRAGMENT_CURRENCY_CODE_MATCHER =
			allOf(
				withId(R.id.lblRateItemCurrencyCode),
				ViewGroupMatchers.childAtPosition(
					ViewGroupMatchers.childAtPosition(
						withId(R.id.rvRates),
						0
					),
					2
				),
				isDisplayed()
			)
		private val CONVERTER_FRAGMENT_AMOUNT_MATCHER =
			allOf(
				withId(R.id.etRateItemQuote),
				ViewGroupMatchers.childAtPosition(
					ViewGroupMatchers.childAtPosition(
						ViewGroupMatchers.childAtPosition(
							ViewGroupMatchers.childAtPosition(
								withId(R.id.rvRates),
								1
							),
							4
						),
						0
					),
					0
				),
				isDisplayed()
			)
	}

	/**
	 * Checks if RecyclerView has results or not
	 */
	inner class CurrencyListInstruction : Instruction() {
		
		override fun getDescription(): String {
			return "Wait for currency list"
		}

		override fun checkCondition(): Boolean {
			val activity =
				(InstrumentationRegistry.getInstrumentation().targetContext.applicationContext
						as TestApp).getCurrentActivity()
					?: return false
			val navHostFragment =
				activity.supportFragmentManager.findFragmentById(R.id.navHostFragmentRevolut)
			val fragments = navHostFragment?.childFragmentManager?.fragments
			fragments?.let {
				val lastFragment =
					navHostFragment.childFragmentManager.fragments[fragments.size - 1]
				val rv = lastFragment.view?.findViewById<RecyclerView>(R.id.rvRates)
				return rv?.let { it.size > 0 } ?: false
			} ?: return false
		}
	}

}
package com.babestudios.revolut.ui

import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.babestudios.revolut.App
import com.babestudios.revolut.R
import com.babestudios.revolut.data.model.CurrencyRates
import com.babestudios.revolut.di.AndroidTestAppComponent
import io.mockk.every
import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RevolutActivityTest {

	@Suppress("BooleanLiteralArgument")
	@Rule
	@JvmField
	var activityTestRule = ActivityTestRule(RevolutActivity::class.java, true, false)

	private fun currencyRates() = CurrencyRates(
		rates = hashMapOf(Pair("AUD", 1.583f), Pair("BGN", 1.957f), Pair("BRL", 4.232f))
	)

	private fun currencyRatesForAud() = CurrencyRates(
		rates = hashMapOf(Pair("BGN", 1.26f), Pair("BRL", 2.961f), Pair("EUR", 0.643f))
	)

	private val debounce = (InstrumentationRegistry.getInstrumentation()
		.targetContext.applicationContext as App)
		.resources
		.getInteger(R.integer.inputFieldDebounce)
		.toLong()

	@Before
	fun setUp() {
		val instrumentation = InstrumentationRegistry.getInstrumentation()
		val app = instrumentation.targetContext.applicationContext as App
		val comp = app.provideAppComponent() as AndroidTestAppComponent

		every {
			comp.revolutRepository().baseCurrency()
		} returns ("EUR")
		every {
			comp.revolutRepository().fetchRates("EUR")
		} returns (Observable.just(currencyRates()))
		every {
			comp.revolutRepository().fetchRates("AUD")
		} returns (Observable.just(currencyRatesForAud()))
		activityTestRule.launchActivity(Intent())
	}

	@Test
	fun whenConverterScreenStarts_thenCurrencyListIsDisplayed() {
		RevolutRobot(debounce)
			.assertCurrencyListIsDisplayed()
	}

	@Test
	fun whenBaseAmountChanged_thenRatesAreUpdated() {
		RevolutRobot(debounce)
			.setAmount()
			.assertRatesAreUpdated()
	}

	@Test
	fun whenCurrencyClicked_thenListIsUpdated() {
		RevolutRobot(debounce)
			.clickRateItem()
			.assertVisitablesReordered()
	}

}

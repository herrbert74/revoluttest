package com.babestudios.revolut

import com.babestudios.revolut.di.AndroidTestAppComponent
import com.babestudios.revolut.di.AndroidTestAppModule
import com.babestudios.revolut.di.AppComponent
import com.babestudios.revolut.di.DaggerAndroidTestAppComponent
import com.babestudios.revolut.ui.AppNavigation

open class TestApp : App() {

	private lateinit var testAppComponent: AndroidTestAppComponent

	override fun provideAppComponent(): AppComponent {

		if (!this::testAppComponent.isInitialized) {
			testAppComponent = DaggerAndroidTestAppComponent
				.factory()
				.create(AndroidTestAppModule(this), AppNavigation(), this)
		}
		return testAppComponent
	}
}

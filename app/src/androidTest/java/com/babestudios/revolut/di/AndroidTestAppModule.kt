package com.babestudios.revolut.di

import android.content.Context
import com.babestudios.base.data.AnalyticsContract
import com.babestudios.base.rxjava.ErrorResolver
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.revolut.data.RevolutErrorResolver
import com.babestudios.revolut.data.RevolutRepositoryContract
import com.babestudios.revolut.data.RevolutService
import dagger.Module
import dagger.Provides
import io.mockk.every
import io.mockk.mockk
import javax.inject.Singleton

@Module
class AndroidTestAppModule(context: Context) : AppModule(context) {

	//region Mocks/overrides from BinderModules

	@Provides
	@Singleton
	internal fun provideRevolutRepositoryContract(
		revolutService: RevolutService,
		analytics: AnalyticsContract,
		schedulerProvider: SchedulerProvider
	): RevolutRepositoryContract {
		val mockRevolutRepository = mockk<RevolutRepositoryContract>(relaxed = true)

		every {
			mockRevolutRepository.logAppOpen()
		} returns Unit

		every {
			mockRevolutRepository.logScreenView(any())
		} returns Unit

		every {
			mockRevolutRepository.logSearch(any())
		} returns Unit

		return mockRevolutRepository
	}

	@Provides
	@Singleton
	internal fun provideErrorResolver(): ErrorResolver {
		return mockk<RevolutErrorResolver>()
	}

	//endregion
}
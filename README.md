# README #

## Highlights

* I used MVI with MvRx, which replaces LiveData with an immutable State management.
* I used Retrofit 2, RxJava 2, Dagger 2, AndroidX, Navigation and Material components.
* ErrorResolver interface gives you basic solution for handle standard error messages,
but also gives you flexibility across all projects for other cases. It also removes a lot of
boilerplate in the repositories.
* ViewModel is Unit tested.
* Espresso tests to cover some situations. I used the Robot pattern for them.

## Structure

* Note the use of buildSrc, the standard way to add dependencies with Gradle. In a modularized
  setting I would also add reusable Gradle plugins here, so that the module build files are tiny.
* Most of the styles, base classes, etc. are coming from BaBeStudiosBase, my base library. I
overrode it in a few instances.
* Lately I use a modular structure, but for this app I use only packages instead.
* Common classes within the app go to the common package (or module). I often extract classes from
here to the base library, if I need them elsewhere.
* Dependency related files go to the 'di' package. The Dagger component can be obtained from the
App class. No Component dependencies were added for this small project. I access the component from
the Activity, from here the ViewModel can be injected such, that they don't know anything about
each other.
* Navigation related files go to the navigation package. This separation was needed for
modularization only, but I kept it for consistency.
* Data package contains everything related to networking and data storage.
* Views, ViewModels and State go into the ui package. There is one Activity per feature, the
screens are represented by Fragments. There is one ViewModel per Activity, which also holds
the State.
* Lists are simplified/standardized by using the Factory and Visitable patterns, where the
ViewHolder types are represented by their layouts. This makes them easily extendable. I added
DiffUtil callbacks for each list.

## Room for improvement

* Proper storage and decoupling of it. I learnt SqlDelight with this assignment. Probably I will
need more work on mitigating problems with the increased complexity. One area definitely needs
improvement is error handling. My ErrorResolver interface and the way it works in the repository
doesn't work well with storage, because it's now too coupled.
* More tests, better setup for test (less repetition, etc.)
* I used MultiStateView to represent view states. I understand this is not always ideal, because
its loading and error states are disruptive. Only the initial loading is displayed. I will review
this library in the future.
* Etc.

## A note on production readiness

* In my opinion an app is production ready when the product owner (in this case the interviewer)
finds it sufficient. So by definition I cannot know when this is true. I understand in this case my
definition of done is tested, so I tried to add everything I can in a timely manner.
However, I still feel this is not production ready, because it needs at least a second pair of eyes
and a confirmation of readiness from that. Once this happens, I'm more confident.

## Other notes

* MvRx doesn't support Navigation component lifecycle fully at the moment, so I had to handle
orientation changes manually. See BaseFragment and its implementations.
